import org.springframework.data.mongodb.core.aggregation.ArrayOperators;

import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Map;

class Solution {
    public static void main(String[] args) {
        /*int nums[] = {1,12,1,23,134,1,0,0};
        int[] ints = twoPlus1(nums, 1);
        for (int i:ints) {
            System.out.println(i);
        }*/
        ListNode node1 = new ListNode();
        node1.add(0,1);
        node1.add(1,2);
        node1.add(2,3);
        ListNode node2 = new ListNode();
        node2.add(0,1);
        node2.add(1,2);
        node2.add(2,3);
        Solution solution = new Solution();
        ListNode node = solution.addTwoNumbers(node1, node2);
        System.out.println(node.element());
        //321+321 = 642 // 246
    }

    static class ListNode extends LinkedList<Integer>{

    }
    public ListNode addTwoNumbers(ListNode l1, ListNode l2) {
        Integer first1 = l1.getFirst();
        Integer last1 = l1.getLast();
        l1.set(0, last1);
        l1.set(2, first1);
        Iterator<Integer> iterator = l1.iterator();
        String str = "";
        while (iterator.hasNext()) {
            Integer next = iterator.next();
            str += next;
        }
        Integer last = l2.getLast();
        Integer first = l2.getFirst();
        l2.set(0, last);
        l2.set(2, first);
        Iterator<Integer> iterator2 = l2.iterator();
        String str2 = "";
        while (iterator2.hasNext()) {
            Integer next = iterator2.next();
            str2 +=next;
        }

        int i1 = Integer.parseInt(str);
        int i2 = Integer.parseInt(str2);
        int i = i1 + i2;
        String value = i+"";
        ListNode node = new ListNode();
        char c = value.charAt(0);
        char c1 = value.charAt(1);
        char c2 = value.charAt(2);
        node.add(Integer.parseInt(c2+""));
        node.add(Integer.parseInt(c1+""));
        node.add(Integer.parseInt(c+""));
        return node;
    }

    public static int[] twoSum(int[] nums, int target) {
        int arrLength = nums.length;
        int arr[] = new int[2];
        for(int i=0;i<arrLength;i++){
            for(int j=i+1; j<arrLength; j++) {
                if(nums[i]+nums[j]==target) {
                    arr[0] = i;
                    arr[1] = j;
                    return arr;
                }
            }
        }
        return arr;
    }

    public static int[] twoPlus(int arr[], int target) {
        Map<Integer, Integer> map = new HashMap<>();
        for(int i=0; i<arr.length; i++) {
            map.put(arr[i],i);
        }

        for(int i=0; i<arr.length; i++) {
            Integer i1 = target - arr[i];
            if(map.containsKey(i1) && i != map.get(i1)) {
                int arr1[] = new int[2];
                arr1[0] = i;arr1[1] = map.get(i1);
                return arr1;
            }
        }
        throw new IllegalArgumentException("");
    }

    public static int[] twoPlus1(int arr[], int target) {
        Map<Integer, Integer> map = new HashMap<>();
        for(int i=0; i<arr.length; i++) {
            Integer i1 = target - arr[i];
            if(map.containsKey(i1)) {
                return new int[]{map.get(i1), i};
            }
            map.put(arr[i], i);
        }
        throw new IllegalArgumentException("");
    }


}