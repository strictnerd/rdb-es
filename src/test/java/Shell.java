/**
 * Created by clq on 2019/3/3.
 */
public class Shell {
    public static void main(String[] args) {
        sort();
    }

    public static void sort() {
        int arr[] = {234,1,23,123,2,1123,41,2,3,1,2,23,2};

        int n = arr.length;
        int h = 1;

        while (h<=n/5) {
            h = 5*h + 1;
        }

        while (h>=1) {
            for(int i=0; i<n; i+=h) {
                int j;
                for(j=i; j>=h&&arr[j] > arr[j-h]; j-=h) {
                    int temp = arr[j];
                    arr[j] = arr[j-h];
                    arr[j-h] = temp;
                }
            }
            h=h/5;
        }

        for (int a:arr) {
            System.out.println(a);
        }
    }
}
