import com.mongodb.util.JSON;
import org.nutz.json.Json;

import java.net.Socket;
import java.util.*;

/**
 * Created by clq on 2019/3/3.
 */
public class M {
    public static void main(String[] args) {
        Socket socket = new Socket();
        System.out.println(Json.toJson(socket));
        HashMap map = new HashMap();
        map.put(null, null);
        Hashtable hashtable = new Hashtable();
        //hashtable.put(null, null);
        ArrayList list = new ArrayList();
        LinkedList list1 = new LinkedList();
        Vector vector = new Vector();
        //list.get(1);
        int arr[] = {234,1,23,123,2,1123,41,2,3,1,2,23,2};
        sort(arr);
        for (int i:arr
             ) {
            System.out.println(i);
        }
    }

    public static void sort(int arr[]) {
        int temp[] = new int[arr.length];
        merge(arr, 0, arr.length-1, temp);
    }

    public static void merge(int arr[], int left, int right, int temp[]) {
        int middle = (left+right)/2;
        if(left<right) {
            merge(arr, left, middle, temp);
            merge(arr, middle+1, right, temp);
            sort(arr, left, middle, right, temp);
        }
    }

    public static void sort(int arr[], int left, int middle, int right, int temp[]) {
        int i=left;
        int j=middle+1;
        int t=0;
        while (i<=middle && j <= right) {
            if(arr[i] > arr[j]) {
                temp[t++] = arr[i++];
            }else {
                temp[t++] = arr[j++];
            }
        }

        while (i <= middle) {
            temp[t++] = arr[i++];
        }

        while (j <= right) {
            temp[t++] = arr[j++];
        }
        t=0;
        while (left <= right) {
            arr[left++] = temp[t++];
        }
    }
}
