package home;

import edu.princeton.cs.algs4.*;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.PriorityQueue;
import java.util.Queue;
import java.util.Scanner;

/**
 * Created by clq on 2019/4/19.
 */

public class TopM {
    /**
     * 从N个输入中找到M个最大的元素
     * @param args
     */
    public static void main(String[] args) throws IOException {
        int M = Integer.parseInt(args[0]);
        MinPQ<Transaction> pq = new MinPQ<>(M+1);
        //MaxPQ
        while (StdIn.hasNextLine()) {
            pq.insert(new Transaction(StdIn.readLine()));
            if(pq.size() > M) {
                pq.delMin();
            }
            Stack<Transaction> stack = new Stack<>();
            while (!pq.isEmpty()) {
                stack.push(pq.delMin());
                for (Transaction r:stack
                     ) {
                    StdOut.println(r);
                }
            }
        }
    }
}
