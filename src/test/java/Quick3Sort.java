/**
 * Created by clq on 2019/3/12.
 */
public class Quick3Sort {

    public static void main(String[] args) {
        Comparable comparable[] = {1,2,3,4,5,6,7,8,9,0};
        sort(comparable, 0, comparable.length-1);
        for (Comparable c : comparable) {
            System.out.println(c);
        }
    }/*
1,1234,0,12
1,0,1234,12
1,0,12,1234
0,1,12,1234*/
    public static void sort(Comparable a[], int lo, int hi) {
        if(lo>hi) {return;}
        int it=lo,i=lo+1,gt=hi;
        Comparable v = a[lo];
        while (i<=gt) {
            int cmp = a[i].compareTo(v);
            if(cmp<0) {exch(a, it++, i++);}
            else if (cmp>0) {exch(a, i, gt--);}
            else {i++;}
            for (Comparable c : a) {
                System.out.print(c+",");
            }
            System.out.println("i:"+i+"，it："+it+",gt:"+gt);
        }
        sort(a, lo, it-1);
        sort(a, gt+1, hi);
    }



    public static void exch(Comparable[] a, int i, int j) {
        Comparable temp = a[i];
        a[i] = a[j];
        a[j] = temp;
    }


}
