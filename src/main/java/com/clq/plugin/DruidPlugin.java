package com.clq.plugin;

import com.alibaba.druid.pool.DruidDataSource;
import org.springframework.boot.autoconfigure.jdbc.DataSourceBuilder;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;

import javax.sql.DataSource;
import java.sql.SQLException;

@Component
public class DruidPlugin {

	/**
	 * 多数据源配置
	 * @return
	 * @throws SQLException
	 */
	@Bean(name = "ds")
	@ConfigurationProperties(prefix = "t.data")
	public DataSource dataSource() throws SQLException {
		DataSource build = DataSourceBuilder.create().type(DruidDataSource.class).build();
		return build;
	}

	/**
	 * 默认数据源配置
	 * @return
	 * @throws SQLException
	 */
	@Primary
	@Bean
	@ConfigurationProperties(prefix = "t1.data1")
	public DataSource dataSource1() throws SQLException {
		DataSource build = DataSourceBuilder.create().type(DruidDataSource.class).build();
		return build;
	}

}