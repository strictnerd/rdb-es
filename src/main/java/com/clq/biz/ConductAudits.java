package com.clq.biz;

import org.nutz.dao.entity.annotation.Column;
import org.nutz.dao.entity.annotation.Id;
import org.nutz.dao.entity.annotation.Table;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldIndex;
import org.springframework.data.elasticsearch.annotations.FieldType;

import java.io.Serializable;
import java.sql.Date;
import java.sql.Timestamp;

@Table("t_tsps_conduct_audits")
@Document(indexName = "hlj-audit", type = "test")
public class ConductAudits implements Serializable{
	@Column("c_id")
	private String id;
	@Column("n_person_id")
	private Long personId;
	@Column("n_dept_id")
	private Long deptId;
	@Column("c_code")
	private String code;
	@Column("c_name")
	private String name;
	/**
	 * 所属部门
	 */
	@Column("c_dept")
	private String dept;
	/**
	 * 日志上报时间
	 */
	@Column("n_log_time")
	private Long logTime;
	/**
	 */
	@Column("c_log_type")
	private Integer logType;
	/**
	 * 操作内容
	 */
	@Column("c_content")
	private String content;
	/**
	 * 操作结果0:被禁止，1：成功
	 */
	@Column("c_result")
	private String result;
	/**
	 * 证书sn
	 */
	@Column("c_sn")
	private String sn;
	/**
	 * 身份证号
	 */
	private String sfzh;
	/**
	 */
	@Column("c_create_time")
	private Timestamp createTime;
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDept() {
		return dept;
	}
	public void setDept(String dept) {
		this.dept = dept;
	}
	
	public Integer getLogType() {
		return logType;
	}
	public void setLogType(Integer logType) {
		this.logType = logType;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public String getResult() {
		return result;
	}
	public void setResult(String result) {
		this.result = result;
	}
	public String getSn() {
		return sn;
	}
	public void setSn(String sn) {
		this.sn = sn;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public Long getPersonId() {
		return personId;
	}
	public void setPersonId(Long personId) {
		this.personId = personId;
	}
	public Long getDeptId() {
		return deptId;
	}
	public void setDeptId(Long deptId) {
		this.deptId = deptId;
	}
	public Long getLogTime() {
		return logTime;
	}
	public void setLogTime(Long logTime) {
		this.logTime = logTime;
	}

	public Timestamp getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Timestamp createTime) {
		this.createTime = createTime;
	}

	public String getSfzh() {
		return sfzh;
	}

	public void setSfzh(String sfzh) {
		this.sfzh = sfzh;
	}
}
