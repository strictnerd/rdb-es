package com.clq.biz;

import org.nutz.dao.Dao;
import org.nutz.dao.impl.NutDao;
import org.nutz.ioc.loader.annotation.Inject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.elasticsearch.core.ElasticsearchTemplate;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.stereotype.Component;

/**
 * Created by Administrator on 2018/12/19.
 */
@Component
public class Log {
    Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    NutDao dao;

    @Autowired
    @Qualifier("sdao")
    NutDao sdao;


    @Autowired
    private ElasticsearchTemplate template;

    @Autowired
    private MongoTemplate mongoTemplate;

    public int count() {
        return dao.count(ConductAudits.class);
    }

    public int count1()
    {
        //template.scroll()
        return sdao.count(ConductAudits.class);
    }

    public void insert(Customer customer) {
        mongoTemplate.save(customer);
    }


}
