package com.clq.core;

import org.quartz.Job;

/**
 * Created by clq on 2019/1/5.
 */
public abstract class AbstractExchanger implements Job{
    public abstract String getCron();
    public abstract Boolean getStatus();
}
