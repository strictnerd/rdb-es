package com.clq.annotations;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Target;

/**
 * Created by clq on 2019/1/25.
 */
@Target({ElementType.METHOD, ElementType.PARAMETER, ElementType.FIELD})
@Documented
public @interface LogAspect {
}
