package com.clq.job;

import com.clq.core.AbstractExchanger;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.stereotype.Component;

/**
 * Created by clq on 2019/1/5.
 */
@Component
public class DataJob extends AbstractExchanger{
    @Override
    public String getCron() {
        return "0/5 * * ? * *";
    }

    @Override
    public Boolean getStatus() {
        return true;
    }

    @Override
    public void execute(JobExecutionContext jobExecutionContext) throws JobExecutionException {
        System.out.println("Test:" + System.currentTimeMillis());
    }
}
