package com.clq.job;

import com.clq.biz.Customer;
import com.clq.biz.Log;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

/**
 * Created by clq on 2018/12/24.
 */
@Component
public class Ora11gToES {
    @Resource
    Log log;

    @Scheduled(fixedRate = 500)
    public void excuteTask() throws IOException {
        InputStream resourceAsStream = this.getClass().getResourceAsStream("/partition");
        BufferedReader br = new BufferedReader(new InputStreamReader(resourceAsStream));
        String line = null;
        String content = "";
        while ((line = br.readLine()) != null) {
            content += line;
        }
        br.close();
        int count = log.count();
        int i = log.count1();
        Customer customer = new Customer();
        customer.firstName = "123";
        customer.lastName = "陈龙泉";
        log.insert(customer);
    }
}
