# rdb-es

#### 介绍
本项目旨在把oracle中的数据摆渡到ES中。目前只实现elasticsearch2.x的数据同步。

#### 软件架构
springboot1.x, druid, nutz,es2.x, oracle11g并且支持多数据源


#### 安装教程

1. git clone https://gitee.com/strictnerd/rdb-es.git
2. 作为maven工程导入idea或者eclpse
3. clean install
4. Application main函数运行

#### 使用说明

1. https://my.oschina.net/u/1787735/blog/2994218

#### 参与贡献

1. Fork 本仓库
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request


#### 码云特技

1. 使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2. 码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3. 你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4. [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5. 码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6. 码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)